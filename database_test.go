package notion_api

import (
	"context"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestClient_QueryDatabase(t *testing.T) {
	type args struct {
		ctx         context.Context
		databaseId  string
		startCursor string
		pageSize    int64
		sorts       []QuerySort
		filter      *QueryFilterProperty
	}
	tests := []struct {
		name       string
		args       args
		wantMethod string
		wantBody   []byte
		want       *DatabaseQueryList
		wantErr    bool
	}{
		{
			name: "Query",
			args: args{
				ctx:         context.Background(),
				databaseId:  "123",
				startCursor: "",
				pageSize:    1,
				sorts:       nil,
				filter: &QueryFilterProperty{
					Property: "Strava ID",
					Number: &QueryFilterNumber{
						Equals: 456,
					},
				},
			},
			wantMethod: http.MethodPost,
			wantBody:   []byte(`{"page_size":1,"filter":{"property":"Strava ID","number":{"equals":456}}}`),
			want:       &DatabaseQueryList{},
			wantErr:    false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				assert.Equal(t, tt.wantMethod, r.Method)
				assert.Equal(t, "/databases/"+tt.args.databaseId+"/query", r.URL.String())
				reqBody, _ := ioutil.ReadAll(r.Body)
				assert.Equal(t, tt.wantBody, reqBody)
				w.WriteHeader(http.StatusOK)
				w.Write([]byte(`{}`))
			}))
			c := NewClient("test-token", WithBaseURL(srv.URL))
			got, err := c.QueryDatabase(tt.args.ctx, tt.args.databaseId, tt.args.startCursor, tt.args.pageSize, tt.args.sorts, tt.args.filter)
			if tt.wantErr {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
			assert.Equal(t, tt.want, got)
		})
	}
}
