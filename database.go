package notion_api

import (
	"context"
	"encoding/json"
)

type DatabaseQueryList struct {
	Object     string `json:"object"`
	Results    []Page `json:"results"`
	HasMore    bool   `json:"has_more"`
	NextCursor string `json:"next_cursor"`
}

type QueryDatabaseRequest struct {
	StartCursor string               `json:"start_cursor,omitempty"`
	PageSize    int64                `json:"page_size,omitempty"`
	Sorts       []QuerySort          `json:"sorts,omitempty"`
	Filter      *QueryFilterProperty `json:"filter,omitempty"`
}

type QuerySort struct {
	Property  string `json:"property,omitempty"`
	Direction string `json:"direction,omitempty"`
}

type QueryFilterText struct {
	Contains string `json:"contains,omitempty"`
	Equals   string `json:"equals,omitempty"`
}

type QueryFilterNumber struct {
	Equals int64 `json:"equals,omitempty"`
}

type QueryFilterProperty struct {
	Or       []QueryFilterProperty `json:"or,omitempty"`
	And      []QueryFilterProperty `json:"and,omitempty"`
	Property string                `json:"property"`
	Text     *QueryFilterText      `json:"text,omitempty"`
	Number   *QueryFilterNumber    `json:"number,omitempty"`
}

func (c *Client) QueryDatabase(
	ctx context.Context,
	databaseId,
	startCursor string,
	pageSize int64,
	sorts []QuerySort,
	filter *QueryFilterProperty,
) (*DatabaseQueryList, error) {
	queryReq := QueryDatabaseRequest{
		StartCursor: startCursor,
		PageSize:    pageSize,
		Sorts:       sorts,
		Filter:      filter,
	}
	queryReqJson, _ := json.Marshal(queryReq)

	body, err := c.Post(ctx, "/databases/"+databaseId+"/query", queryReqJson)
	if err != nil {
		return nil, err
	}

	result := &DatabaseQueryList{}
	err = json.Unmarshal(body, result)
	if err != nil {
		return nil, err
	}

	return result, nil
}
