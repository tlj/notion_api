package notion_api

import (
	"context"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"
)

type CacherMock struct {
	mock.Mock
}

func (c *CacherMock) Get(key string) []byte {
	args := c.Called(key)
	ret := args.Get(0)
	if ret == nil {
		return nil
	}
	return ret.([]byte)
}

func (c *CacherMock) Set(key string, data []byte, ttl time.Duration) error {
	args := c.Called(key, data, ttl)
	return args.Error(0)
}

func TestClient_get(t *testing.T) {
	notionURL := "/test"
	notionMethod := http.MethodGet
	notionToken := "test-token"
	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, notionURL, r.URL.String())
		assert.Equal(t, notionMethod, r.Method)
	}))
	defer svr.Close()
	c := NewClient(notionToken, WithBaseURL(svr.URL))
	_, err := c.Get(context.Background(), notionURL)
	assert.NoError(t, err)
}

func TestClient_getWithEmptyCache(t *testing.T) {
	notionURL := "/test"
	notionMethod := http.MethodGet
	notionToken := "test-token"
	cache := &CacherMock{}
	cache.On("Get", "/test--").Return(nil)
	cache.On("Set", "/test--", []byte{}, 10*time.Second).Return(nil)

	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, notionMethod, r.Method)
		assert.Equal(t, notionURL, r.URL.String())
	}))
	defer svr.Close()
	c := NewClient(notionToken, WithBaseURL(svr.URL), WithCache(cache))
	_, err := c.Get(context.WithValue(context.Background(), "cache", 10*time.Second), notionURL)
	assert.NoError(t, err)

	cache.AssertExpectations(t)
}

func TestClient_getWithCache(t *testing.T) {
	notionURL := "/test"
	notionToken := "test-token"
	cache := &CacherMock{}
	cache.On("Get", "/test--").Return([]byte(`{}`))

	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		t.Error("Expected no http request, due to cache.")
	}))
	defer svr.Close()
	c := NewClient(notionToken, WithBaseURL(svr.URL), WithCache(cache))
	_, err := c.Get(context.WithValue(context.Background(), "cache", 10*time.Second), notionURL)
	assert.NoError(t, err)

	cache.AssertExpectations(t)
}

func TestClient_post(t *testing.T) {
	notionURL := "/test"
	notionMethod := http.MethodPost
	notionToken := "test-token"
	notionPostData := []byte(`{"data":["test"]}`)
	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, notionMethod, r.Method)
		assert.Equal(t, notionURL, r.URL.String())
		assert.Equal(t, "application/json", r.Header.Get("Content-Type"))

		data, _ := ioutil.ReadAll(r.Body)
		assert.Equal(t, notionPostData, data)
	}))
	defer svr.Close()
	c := NewClient(notionToken, WithBaseURL(svr.URL))
	_, err := c.Post(context.Background(), notionURL, notionPostData)
	assert.NoError(t, err)
}

func TestClient_patch(t *testing.T) {
	notionURL := "/test"
	notionMethod := http.MethodPatch
	notionToken := "test-token"
	notionPostData := []byte(`{"data":["test"]}`)
	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, notionMethod, r.Method)
		assert.Equal(t, notionURL, r.URL.String())
		assert.Equal(t, "application/json", r.Header.Get("Content-Type"))

		data, _ := ioutil.ReadAll(r.Body)
		assert.Equal(t, notionPostData, data)
	}))
	defer svr.Close()
	c := NewClient(notionToken, WithBaseURL(svr.URL))
	_, err := c.Patch(context.Background(), notionURL, notionPostData)
	assert.NoError(t, err)
}

func TestClient_do(t *testing.T) {
	notionToken := "test-token"
	notionVersion := "2021-01-01"
	notionMethod := http.MethodGet
	notionURL := "/test"
	notionStatusCode := http.StatusOK
	notionData := []byte(`{"data":[]}`)

	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, notionMethod, r.Method)
		assert.Equal(t, notionURL, r.URL.String())
		assert.Equal(t, "Bearer "+notionToken, r.Header.Get("Authorization"))
		assert.Equal(t, notionVersion, r.Header.Get("Notion-Version"))
		assert.Equal(t, "application/json", r.Header.Get("Content-Type"))

		w.WriteHeader(notionStatusCode)
		w.Write(notionData)
	}))
	defer svr.Close()

	c := NewClient(notionToken, WithBaseURL(svr.URL), WithVersion(notionVersion))
	data, err := c.do(context.Background(), notionMethod, notionURL, nil)
	assert.NoError(t, err)
	assert.Equal(t, notionData, data)
}
