package notion_api

import (
	"context"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestPage_AddTitle(t *testing.T) {
	p := NewPage()
	p.AddTitle("name", "Test Title")

	expected := NewPage()
	expected.Properties["name"] = &Property{
		Title: []Property{
			{
				Type: "text",
				Text: &TextContentProperty{
					Content: "Test Title",
				},
			},
		},
	}

	assert.Contains(t, p.Properties, "name")
	assert.Equal(t, expected.Properties["name"], p.Properties["name"])
}

func TestPage_AddDate(t *testing.T) {
	p := NewPage()
	p.AddDate("created", "2021-01-05")

	expected := NewPage()
	expected.Properties["created"] = &Property{
		Type: "date",
		Date: &DateStartEndProperty{
			Start: "2021-01-05",
		},
	}

	assert.Contains(t, p.Properties, "created")
	assert.Equal(t, expected.Properties["created"], p.Properties["created"])
}

func TestPage_AddURL(t *testing.T) {
	p := NewPage()
	p.AddURL("homepage", "https://example")

	expected := NewPage()
	expected.Properties["homepage"] = &Property{
		Type: "url",
		URL:  "https://example",
	}

	assert.Contains(t, p.Properties, "homepage")
	assert.Equal(t, expected.Properties["homepage"], p.Properties["homepage"])
}

func TestPage_AddHeading2(t *testing.T) {
	p := NewPage()
	p.AddHeading2("A Heading")

	assert.Equal(t, 1, len(p.Children))
	assert.Equal(t, BlockTypeHeading2, p.Children[0].Type)
	assert.Equal(t, "A Heading", p.Children[0].Heading2.Text[0].Text.Content)
}

func TestPage_AddParagraph(t *testing.T) {
	p := NewPage()
	p.AddParagraph("Some Paragraph Text")

	assert.Equal(t, 1, len(p.Children))
	assert.Equal(t, BlockTypeParagraph, p.Children[0].Type)
	assert.Equal(t, "Some Paragraph Text", p.Children[0].Paragraph.Text[0].Text.Content)
}

func TestPage_AddMultiselect(t *testing.T) {
	p := NewPage()
	p.AddMultiselect("sometype", "somevalue")
	p.AddMultiselect("sometype", "othervalue")

	assert.Contains(t, p.Properties, "sometype")
	assert.Equal(t, "multi_select", p.Properties["sometype"].Type)
	assert.Equal(t, 2, len(p.Properties["sometype"].MultiSelectOptions))
}

func TestPage_AddSelect(t *testing.T) {
	p := NewPage()
	p.AddSelect("sometype", "somevalue")

	assert.Contains(t, p.Properties, "sometype")
	assert.Equal(t, "select", p.Properties["sometype"].Type)
	assert.Equal(t, "somevalue", p.Properties["sometype"].Select.Name)
}

func TestPage_AddCheckbox(t *testing.T) {
	p := NewPage()
	p.AddCheckbox("acheckbox", true)

	assert.Contains(t, p.Properties, "acheckbox")
	assert.Equal(t, "checkbox", p.Properties["acheckbox"].Type)
	assert.Equal(t, true, p.Properties["acheckbox"].Checkbox)
}

func TestPage_AddNumber(t *testing.T) {
	p := NewPage()
	p.AddNumber("zero", 0)
	assert.NotContains(t, p.Properties, "zero", "Zero value should not be added.")

	p.AddNumber("one", 1)
	assert.Contains(t, p.Properties, "one")
	assert.Equal(t, "number", p.Properties["one"].Type)
	assert.Equal(t, float64(1), p.Properties["one"].Number)
}

func TestPage_AddRichText(t *testing.T) {
	p := NewPage()
	p.AddRichText("sometext", "This is rich text")

	assert.Contains(t, p.Properties, "sometext")
	assert.Equal(t, "rich_text", p.Properties["sometext"].Type)
	assert.Equal(t, "This is rich text", p.Properties["sometext"].RichText[0].Text.Content)
}

func TestPage_AddRelation(t *testing.T) {
	pageName := "relatedpage"
	value := "12345"

	p := NewPage()
	p.AddRelation(pageName, value)

	assert.Contains(t, p.Properties, pageName)
	assert.Equal(t, p.Properties[pageName].Type, "relation")
	assert.Equal(t, p.Properties[pageName].Relation[0].Id, value)
}

func TestClient_RetrievePage(t *testing.T) {
	notionURL := "/pages/123"
	notionMethod := http.MethodGet
	notionReqCount := 0
	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, notionURL, r.URL.String())
		assert.Equal(t, notionMethod, r.Method)

		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"id":"123"}`))
		notionReqCount++
	}))
	defer svr.Close()
	c := NewClient("testtoken", WithBaseURL(svr.URL))
	p, err := c.RetrievePage(context.Background(), "123")

	assert.NoError(t, err)
	assert.Equal(t, 1, notionReqCount)
	assert.Equal(t, "123", p.Id)
}

func TestClient_CreatePage(t *testing.T) {
	notionURL := "/pages"
	notionMethod := http.MethodPost
	notionReqCount := 0
	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, notionURL, r.URL.String())
		assert.Equal(t, notionMethod, r.Method)

		expectedBody := []byte(`{"id":"123","parent":{"database_id":""},"properties":{}}`)
		body, _ := ioutil.ReadAll(r.Body)
		r.Body.Close()
		assert.Equal(t, expectedBody, body)

		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"id":"123"}`))
		notionReqCount++
	}))
	defer svr.Close()
	c := NewClient("testtoken", WithBaseURL(svr.URL))
	p := NewPage()
	p.Id = "123"
	err := c.CreatePage(context.Background(), p)

	assert.NoError(t, err)
	assert.Equal(t, 1, notionReqCount)
}

func TestClient_UpdatePage(t *testing.T) {
	notionURL := "/pages/123"
	notionMethod := http.MethodPatch
	notionReqCount := 0
	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, notionURL, r.URL.String())
		assert.Equal(t, notionMethod, r.Method)

		expectedBody := []byte(`{"id":"123","parent":{"database_id":""},"properties":{}}`)
		body, _ := ioutil.ReadAll(r.Body)
		r.Body.Close()
		assert.Equal(t, expectedBody, body)

		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"id":"123"}`))
		notionReqCount++
	}))
	defer svr.Close()
	c := NewClient("testtoken", WithBaseURL(svr.URL))
	p := NewPage()
	p.Id = "123"
	err := c.UpdatePage(context.Background(), p)

	assert.NoError(t, err)
	assert.Equal(t, 1, notionReqCount)
}

func TestClient_DeletePage(t *testing.T) {
	notionURL := "/pages/123"
	notionMethod := http.MethodPatch
	notionReqCount := 0
	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, notionURL, r.URL.String())
		assert.Equal(t, notionMethod, r.Method)

		expectedBody := []byte(`{"archived":true,"id":"123","parent":{"database_id":""},"properties":{}}`)
		body, _ := ioutil.ReadAll(r.Body)
		r.Body.Close()
		assert.Equal(t, expectedBody, body)

		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"id":"123"}`))
		notionReqCount++
	}))
	defer svr.Close()
	c := NewClient("testtoken", WithBaseURL(svr.URL))
	p := NewPage()
	p.Id = "123"
	err := c.DeletePage(context.Background(), p)

	assert.NoError(t, err)
	assert.Equal(t, 1, notionReqCount)
}
