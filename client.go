package notion_api

import (
	"bytes"
	"context"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

type ClientOption func(*Client)

func WithBaseURL(baseUrl string) ClientOption {
	return func(c *Client) {
		c.baseUrl = baseUrl
	}
}

func WithVersion(version string) ClientOption {
	return func(c *Client) {
		c.version = version
	}
}

func WithCache(cache cacher) ClientOption {
	return func(c *Client) {
		c.cache = cache
	}
}

type cacher interface {
	Get(key string) []byte
	Set(key string, data []byte, ttl time.Duration) error
}

type Client struct {
	baseUrl string
	version string
	token   string
	cache   cacher
}

func NewClient(token string, opts ...ClientOption) *Client {
	c := &Client{
		token:   token,
		baseUrl: "https://api.notion.com/v1",
		version: "2021-08-16",
	}

	for _, opt := range opts {
		opt(c)
	}

	return c
}

func (c *Client) do(ctx context.Context, method, url string, data []byte) ([]byte, error) {
	cacheKey := url + "--" + base64.StdEncoding.EncodeToString(data)

	if c.cache != nil {
		if _, ok := ctx.Value("cache").(time.Duration); ok {
			cachedData := c.cache.Get(cacheKey)
			if cachedData != nil {
				return cachedData, nil
			}
		}
	}

	req, err := http.NewRequest(method, c.baseUrl+url, bytes.NewBuffer(data))
	if err != nil {
		return nil, fmt.Errorf("error while creating notion api request: %s", err.Error())
	}

	req.Header.Add("Authorization", "Bearer "+c.token)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Notion-Version", c.version)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error while doing notion api request: %s", err.Error())
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("error while reading response body: %s", err.Error())
	}
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("error returned from notion_api. Status: %d. Body: %s", resp.StatusCode, body)
	}

	if c.cache != nil {
		if cacheTtl, ok := ctx.Value("cache").(time.Duration); ok {
			_ = c.cache.Set(cacheKey, body, cacheTtl)
		}
	}

	return body, nil
}

func (c *Client) Get(ctx context.Context, url string) ([]byte, error) {
	return c.do(ctx, "GET", url, nil)
}

func (c *Client) Post(ctx context.Context, url string, data []byte) ([]byte, error) {
	return c.do(ctx, "POST", url, data)
}

func (c *Client) Patch(ctx context.Context, url string, data []byte) ([]byte, error) {
	return c.do(ctx, "PATCH", url, data)
}
