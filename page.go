package notion_api

import (
	"context"
	"encoding/json"
	"fmt"
)

type PageParent struct {
	DatabaseID string `json:"database_id"`
}

type TextContentProperty struct {
	Content string `json:"content,omitempty"`
}

type Color string

const (
	ColorDefault = "default"
	ColorGray    = "gray"
	ColorBrown   = "brown"
	ColorOrange  = "orange"
	ColorYellow  = "yellow"
	ColorGreen   = "green"
	ColorBlue    = "blue"
	ColorPurple  = "purple"
	ColorPink    = "pink"
	ColorRed     = "red"
)

type SelectProperty struct {
	Id    string `json:"id,omitempty"`
	Name  string `json:"name,omitempty"`
	Color Color  `json:"color,omitempty"`
}

type FileProperty struct {
	Name     string            `json:"name,omitempty"`
	Type     string            `json:"type,omitempty"`
	External *FilePropertyFile `json:"external,omitempty"`
	File     *FilePropertyFile `json:"file,omitempty"`
}

type FilePropertyFile struct {
	Url        string `json:"url,omitempty"`
	External   bool   `json:"external,omitempty"`
	ExpiryTime string `json:"expiry_time,omitempty"`
}

type Property struct {
	Id                 string                `json:"id,omitempty"`
	Title              []Property            `json:"title,omitempty"`
	Type               string                `json:"type,omitempty"`
	RichText           []Property            `json:"rich_text,omitempty"`
	Text               *TextContentProperty  `json:"text,omitempty"`
	Date               *DateStartEndProperty `json:"date,omitempty"`
	Number             float64               `json:"number,omitempty"`
	Relation           []*Property           `json:"relation,omitempty"`
	PlainText          string                `json:"plain_text,omitempty"`
	URL                string                `json:"url,omitempty"`
	Checkbox           bool                  `json:"checkbox,omitempty"`
	MultiSelectOptions []SelectProperty      `json:"multi_select,omitempty"`
	Select             *SelectProperty       `json:"select,omitempty"`
	Files              []FileProperty        `json:"files,omitempty"`
}

type DateStartEndProperty struct {
	Start string `json:"start,omitempty"`
	End   string `json:"end,omitempty"`
}

type BlockType string

const (
	BlockTypeHeading2  BlockType = "heading_2"
	BlockTypeParagraph BlockType = "paragraph"
)

type BlockText struct {
	Text []Property `json:"text,omitempty"`
}

type BlockContent struct {
	Type string     `json:"type,omitempty"`
	Text *BlockText `json:"text"`
}

type Block struct {
	Object    string     `json:"object,omitempty"`
	Type      BlockType  `json:"type,omitempty"`
	Heading2  *BlockText `json:"heading_2,omitempty"`
	Paragraph *BlockText `json:"paragraph,omitempty"`
}

type Page struct {
	Object         string               `json:"object,omitempty"`
	Archived       bool                 `json:"archived,omitempty"`
	CreatedTime    string               `json:"created_time,omitempty"`
	LastEditedTime string               `json:"last_edited_time,omitempty"`
	Id             string               `json:"id,omitempty"`
	Url            string               `json:"url,omitempty"`
	Parent         PageParent           `json:"parent"`
	Properties     map[string]*Property `json:"properties"`
	Children       []Block              `json:"children,omitempty"`
}

func NewPage() *Page {
	p := &Page{}
	p.Properties = make(map[string]*Property)
	return p
}

func (p *Page) AddHeading2(text string) {
	b := Block{
		Object: "block",
		Type:   BlockTypeHeading2,
	}
	b.Heading2 = &BlockText{
		Text: []Property{
			{
				Type: "text",
				Text: &TextContentProperty{
					Content: text,
				},
			},
		},
	}
	p.Children = append(p.Children, b)
}

func (p *Page) AddParagraph(text string) {
	b := Block{
		Object: "block",
		Type:   BlockTypeParagraph,
	}
	b.Paragraph = &BlockText{
		Text: []Property{
			{
				Type: "text",
				Text: &TextContentProperty{
					Content: text,
				},
			},
		},
	}
	p.Children = append(p.Children, b)
}

func (p *Page) AddTitle(name, title string) {
	p.Properties[name] = &Property{
		Title: []Property{
			{
				Type: "text",
				Text: &TextContentProperty{
					Content: title,
				},
			},
		},
	}
}

func (p *Page) AddDate(name, date string) {
	p.Properties[name] = &Property{
		Type: "date",
		Date: &DateStartEndProperty{
			Start: date,
		},
	}
}

func (p *Page) AddURL(name, url string) {
	p.Properties[name] = &Property{
		Type: "url",
		URL:  url,
	}
}

func (p *Page) AddMultiselect(name string, option string) {
	if _, ok := p.Properties[name]; !ok {
		p.Properties[name] = &Property{
			Type: "multi_select",
		}
	}

	p.Properties[name].MultiSelectOptions = append(p.Properties[name].MultiSelectOptions, SelectProperty{Name: option})
}

func (p *Page) AddSelect(name, option string) {
	p.Properties[name] = &Property{
		Type: "select",
		Select: &SelectProperty{
			Name: option,
		},
	}
}

func (p *Page) AddCheckbox(name string, value bool) {
	p.Properties[name] = &Property{
		Type:     "checkbox",
		Checkbox: value,
	}
}

func (p *Page) AddNumber(name string, number float64) {
	if number == 0 {
		return
	}
	p.Properties[name] = &Property{
		Type:   "number",
		Number: number,
	}
}

func (p *Page) AddRichText(name string, text string) {
	p.Properties[name] = &Property{
		Type: "rich_text",
		RichText: []Property{
			{
				Type: "text",
				Text: &TextContentProperty{
					Content: text,
				},
			},
		},
	}
}

func (p *Page) AddFile(name, filename string, url string) {
	p.Properties[name] = &Property{
		Type: "files",
		Files: []FileProperty{
			{
				Name: filename,
				Type: "external",
				External: &FilePropertyFile{
					Url: url,
				},
			},
		},
	}
}

func (p *Page) AddRelation(name string, relation string) {
	p.Properties[name] = &Property{
		Type: "relation",
		Relation: []*Property{
			{
				Id: relation,
			},
		},
	}
}

func (c *Client) RetrievePage(ctx context.Context, pageId string) (*Page, error) {
	body, err := c.Get(ctx, "/pages/"+pageId)
	if err != nil {
		return nil, err
	}

	result := &Page{}
	err = json.Unmarshal(body, result)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (c *Client) CreatePage(ctx context.Context, page *Page) error {
	str, err := json.Marshal(page)
	if err != nil {
		return fmt.Errorf("error marshalling notion_api page to json: %s", err.Error())
	}

	_, err = c.Post(ctx, "/pages", str)

	return err
}

func (c *Client) UpdatePage(ctx context.Context, page *Page) error {
	str, err := json.Marshal(page)
	if err != nil {
		return fmt.Errorf("error marshalling notion_api page to json: %s", err.Error())
	}

	_, err = c.Patch(ctx, "/pages/"+page.Id, str)

	return err
}

func (c *Client) DeletePage(ctx context.Context, page *Page) error {
	page.Archived = true
	return c.UpdatePage(ctx, page)
}
